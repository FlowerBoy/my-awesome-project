import org.testng.annotations.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PropertiesMakerTest {

    PropertiesMaker propertiesMaker = new PropertiesMaker();

    @Test
   public void validateSuccess() throws Exception{
        propertiesMaker.validate("Hallo");
        assertEquals("","");
    }
    @Test(expectedExceptions = Exception.class)
    public void validateFailure() throws Exception{
        propertiesMaker.validate("lo$%\"ol");
    }
    @Test
    public void square(){
        int solution = propertiesMaker.square(10);
        assertEquals(100,solution);
    }
}