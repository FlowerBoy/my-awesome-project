import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ActionEventEventHandler implements EventHandler<ActionEvent> {
    private final TextField usernameTextfield;
    private final TextField passwordTextfield;
    private final Stage stage;
    private int failureCounter = 0;
    private Alert warning;
    private Alert error;
    private Main main;

    public ActionEventEventHandler(TextField usernameTextfield, TextField passwordTextfield, Stage stage, Main main) {
        this.usernameTextfield = usernameTextfield;
        this.passwordTextfield = passwordTextfield;
        this.stage = stage;
        this.main = main;
    }

    @Override
    public void handle(ActionEvent event) {

        if (main.identify(usernameTextfield.getText(), passwordTextfield.getText())) {
            if (!main.checkUserAuthorized(usernameTextfield.getText())) {
                error = new Alert(Alert.AlertType.ERROR);
                error.setHeaderText("Unauthorized User");
                error.setContentText("User is not allowed to use this tool");
                error.setTitle("Error");
                error.showAndWait();
                stage.close();
                error.close();
            }else {

                WhatToDo whatToDo = new WhatToDo();
                stage.close();
            }

        } else {
            warning = new Alert(Alert.AlertType.WARNING);
            warning.setTitle("Warning");
            warning.setHeaderText("Username or Password wrong");
            if (failureCounter == 0) {
                warning.setContentText("Two attempts remaining");
            } else if (failureCounter == 1) {
                warning.setContentText("One attempt remaining");
            } else {
                warning.setContentText("Zero attempts remaining");
            }
            warning.showAndWait();

            failureCounter++;
            if (failureCounter >= 3) {
                stage.close();
            }

        }
    }
}

