import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class DataInput {

    Label label;
    TextField textfield;

    public DataInput(String name) {
        label = new Label(name);
        textfield = new TextField();
    }

    public Label getLabel() {
        return label;
    }

    public TextField getTextfield() {
        return textfield;
    }
}
