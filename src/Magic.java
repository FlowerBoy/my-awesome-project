@FunctionalInterface
interface Fleiss {
    void tuwas();
}

interface Faul {
    void schlafen(Error kaese);
}

public class Magic {
    public Magic() {

    }

    public static void main(String[] args) {
        new Magic().start();
    }

    private void start() {

        System.out.println("Starte Magic...");

        Fleiss meinFleiss = () ->  System.out.println("Ich tu was und bin fleissig");
        fleissig(meinFleiss);


//        schlafen(new Faul() {
//            @Override
//            public void schlafen(String s) {
//                System.out.println("Ich habe keinen Bock mehr und gehe schlafen");
//            }
//        });

        schlafen(stupsie -> System.out.println("Ich gehe schlafen  "  + stupsie), new Error("jaja"));

    }

    private void fleissig(Fleiss fleiss) {
        fleiss.tuwas();
    }

    private void schlafen(Faul faul, Error s) {
        faul.schlafen(s);
    }
}

