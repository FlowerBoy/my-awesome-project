import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Properties;


public class PropertiesMaker {

    char[] unwantedSigns = {'!', '"', '§', '$', '%', '&', '/',' '};
    String fileNameKeyspace = "KeyspaceProperties.txt";
    String fileNameAccount = "AccountProperties";

    public PropertiesMaker() {
        if(!new File(fileNameKeyspace).exists()){
            createDefault();
        }


    }

    public void createDefault() {

        try {
            Properties properties = new Properties();
            properties.put("Keyspaces", "oauth2_gmx,oauth2_web.de,oauth2_gcom");

            FileOutputStream outTest = new FileOutputStream(fileNameKeyspace);
            properties.store(outTest, "Made by Linus & Sebastian");

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public Properties readProperties() {
        try {
            Properties properties = new Properties();
            FileInputStream inTest = new FileInputStream(fileNameKeyspace);
            properties.load(inTest);
            return properties;

        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    public String getProperty(String key) {
        boolean check = false;
        ArrayList arrayList = new ArrayList();


        return readProperties().getProperty(key);

    }

    public void validate(String s) throws Exception {
        for (int i = 0; i < s.length(); i++) {
            for (int m = 0; m < unwantedSigns.length; m++) {


                if (s.charAt(i) == unwantedSigns[m]) {
                    throw new Exception("Validation failed!");
                }
            }
        }
    }

    int square(int i){
        return i*i;
    }

}

