import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;

public class Main extends Application {


    private String username;
    private String password;
    private ArrayList<String> usernames;
    private ArrayList<String> authorizedUsernames;
    private ArrayList<String> passwords;


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {




        stage.setTitle("OAuth2 Client Tool Login");
        stage.getIcons().add(new Image("/Folder/Tintenfisch.jpeg"));
        stage.getIcons().addAll(new Image("/Folder/Tintenfisch.jpeg"));
        stage.setResizable(false);

        HBox line1 = new HBox(10);
        HBox line2 = new HBox(10);
        HBox line3 = new HBox(10);

        Button okButton = new Button();
        Button cancelButton = new Button();

        Label usernameLabel = new Label();
        Label passwordLabel = new Label();


        PasswordField passwordField = new PasswordField();
        TextField usernameTextfield = new TextField();

        usernameLabel.setText("Username:");
        passwordLabel.setText("Password:");

        okButton.setText("OK");
        cancelButton.setText("Cancel");

        line1.setAlignment(Pos.CENTER);
        line2.setAlignment(Pos.CENTER);
        line3.setAlignment(Pos.CENTER);

        line1.setPadding(new Insets(20));
        line3.setPadding(new Insets(20));

        okButton.setPadding(new Insets(10, 32, 10, 32));
        cancelButton.setPadding(new Insets(10, 20, 10, 20));

        okButton.setOnAction(new ActionEventEventHandler(usernameTextfield, passwordField, stage, this));

        cancelButton.setOnAction(event -> {
            stage.close();
        });


        usernames = new ArrayList<String>(5);
        usernames.add("Linus");
        usernames.add("Max");
        usernames.add("Luis");
        usernames.add("Paul");
        usernames.add("Julian");
        usernames.add("l");

        authorizedUsernames = new ArrayList<String>(5);
        authorizedUsernames.add("Linus");
        authorizedUsernames.add("Max");
        authorizedUsernames.add("l");

        passwords = new ArrayList<String>();
        passwords.add("Linus_password");
        passwords.add("Max_password");
        passwords.add("Luis_password");
        passwords.add("Pauls_password");
        passwords.add("Julians_password");
        passwords.add("l");


        line1.getChildren().addAll(usernameLabel, usernameTextfield);
        line2.getChildren().addAll(passwordLabel, passwordField);
        line3.getChildren().addAll(okButton, cancelButton);

        VBox vBox = new VBox();

        vBox.getChildren().addAll(line1, line2, line3);

        stage.setScene(new Scene(new StackPane(vBox), 400, 200));
        stage.show();

    }

    public boolean identify(String username, String password) {
        boolean check = false;
        for (int i = 0; i < usernames.size(); i++) {
            if (username.equals(usernames.get(i)) && password.equals(passwords.get(i))) {
                check = true;
            }
        }
        return check;
    }

    public boolean checkUserAuthorized(String username) {
        boolean check = false;
        for (int i = 0; i < authorizedUsernames.size(); i++) {
            if (username.equals(authorizedUsernames.get(i))) {
                check = true;
            }
        }
        return check;
    }

}




