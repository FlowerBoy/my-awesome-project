import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Frontend extends Window {

    List<DataInput> dataInputs = new ArrayList<>();
    ComboBox<String> keyspaceComboBox = new ComboBox<>();
    Label keyspaceLabel = new Label("Keyspace");
    Alert alertDelete = new Alert(Alert.AlertType.INFORMATION);
    Alert alertUpdate = new Alert(Alert.AlertType.INFORMATION);
    Alert alertInsert = new Alert(Alert.AlertType.INFORMATION);
    private PropertiesMaker propertiesMaker = new PropertiesMaker();

    int type;




    public Frontend(int type, String keyspaceValue, String clientIDValue) {
        super(1000, 500);
        this.type = type;

        HBox hBox = new HBox(5);
        VBox vBox1 = new VBox(15.4);
        VBox vBox2 = new VBox(5);
        HBox buttonHBox = new HBox(5);


        dataInputs.add(new DataInput("ClientID"));
        dataInputs.add(new DataInput("ClientSecret"));
        dataInputs.add(new DataInput("ClientHash"));
        dataInputs.add(new DataInput("BIParameter"));
        dataInputs.add(new DataInput("Scopes"));
        dataInputs.add(new DataInput("Description"));
        dataInputs.add(new DataInput("Contact"));
        dataInputs.add(new DataInput("Creation Time"));
        dataInputs.add(new DataInput("State"));
        dataInputs.add(new DataInput("Redirect URL"));


        Button insertNewClientIDButton = new Button("Insert new ClientID");
        Button updateClientIDButton = new Button("Update ClientID");
        Button generateClientSecretButton = new Button("Generate ClientSecret");



        dataInputs.get(0).getTextfield().setText(clientIDValue);
        keyspaceComboBox.setValue(keyspaceValue);


        vBox1.getChildren().addAll(keyspaceLabel);
        vBox2.getChildren().addAll(keyspaceComboBox);

        for (DataInput inputs : dataInputs) {
            vBox1.getChildren().add(inputs.getLabel());



            vBox2.getChildren().add(inputs.getTextfield());
            inputs.getTextfield().setPadding(new Insets(5,500,5,0));
        }




        hBox.getChildren().addAll(vBox1, vBox2);
        buttonHBox.getChildren().addAll(insertNewClientIDButton, updateClientIDButton, generateClientSecretButton);
        vBox.getChildren().addAll(buttonHBox, hBox);
        vBox.setSpacing(20);
        vBox.setPadding(new Insets(20, 40, 20, 40));

        keyspaceComboBox.getItems().addAll();

        alertInsert.setContentText("Insert was successful");
        alertInsert.setTitle("Erfolg");
        alertInsert.setHeaderText("Erfolg!");


        alertUpdate.setContentText("Update was successful");
        alertUpdate.setTitle("Erfolg");
        alertUpdate.setHeaderText("Erfolg!");



        alertDelete.setContentText("Delete was successful");
        alertDelete.setTitle("Erfolg");
        alertDelete.setHeaderText("Erfolg!");

        insertNewClientIDButton.setOnAction(event -> {
            System.out.println(convertIntoJSON());

            showAlert();
            stage.close();


        });

        updateClientIDButton.setOnAction(event -> {
            System.out.println(convertIntoJSON());
            showAlert();
            stage.close();
        });

        if (type == 1) {
            insertNewClientIDButton.setDisable(true);
            updateClientIDButton.setDisable(true);
            keyspaceComboBox.getItems().addAll((propertiesMaker.getProperty("Keyspaces").split(",")));
            keyspaceComboBox.setValue("Select Keyspace");
            keyspaceComboBox.setOnAction(event -> {
                if (!keyspaceComboBox.getValue().equals("Select Keyspace")){
                    insertNewClientIDButton.setDisable(false);

                }
            });
        } else if (type == 2) {
            insertNewClientIDButton.setDisable(true);
            generateClientSecretButton.setDisable(true);
        } else {
            insertNewClientIDButton.setText("Delete Selected ClientID");
            updateClientIDButton.setDisable(true);
            generateClientSecretButton.setDisable(true);

        }
    }

    private JSONObject convertIntoJSON() {
        JSONObject object = new JSONObject();
        for (DataInput inputs : dataInputs) {
            object.put(inputs.getLabel().getText(), inputs.getTextfield().getText());
            object.put(keyspaceLabel.getText(), keyspaceComboBox.getValue());
        }

        return object;
    }
    private void showAlert(){
        if (type == 1){
            alertInsert.show();
        }else if (type == 2){
            alertUpdate.show();
        }else {
            alertDelete.show();
        }
    }
}

