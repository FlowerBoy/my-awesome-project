import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.util.ArrayList;


public class KeySpace extends Window {

    ComboBox keyspaceComboBox;
    ComboBox clientIDComboBox;
    private PropertiesMaker propertiesMaker = new PropertiesMaker();

    public KeySpace(int type) {

        super(500,200);
        Label label = new Label();
        Label keyspaceLabel = new Label("Select keyspace");
        Label clientIDLabel = new Label("Select ClientID");
        keyspaceComboBox = new ComboBox();
        clientIDComboBox = new ComboBox();
        Button okButton = new Button("OK");
        Button cancelButton = new Button("Cancel");
        HBox keyspaceHBox = new HBox(5);
        HBox clientIDHBox = new HBox(5);
        HBox buttonHBox = new HBox(5);



        keyspaceHBox.getChildren().addAll(keyspaceLabel, keyspaceComboBox);
        clientIDHBox.getChildren().addAll(clientIDLabel, clientIDComboBox);
        buttonHBox.getChildren().addAll(okButton, cancelButton);
        vBox.getChildren().addAll(label, keyspaceHBox, clientIDHBox, buttonHBox);

        vBox.setSpacing(15);
        vBox.setPadding(new Insets(20,40,20,40));
        label.setPadding(new Insets(0,0,0,20));
        buttonHBox.setPadding(new Insets(0,0,0,20));
        okButton.setPadding(new Insets(10,32,10,32));
        cancelButton.setPadding(new Insets(10,20,10,20));

        okButton.setOnAction(event -> {
            stage.close();
            Frontend frontend = new Frontend(type, getKeyspaceComboBoxValue(), getClientIDComboBoxValue());
        });

        cancelButton.setOnAction(event -> {
            stage.close();
        });

        keyspaceComboBox.getItems().addAll(split(propertiesMaker.getProperty("Keyspaces")));
        clientIDComboBox.getItems().addAll("123456789", "987654321", "4000", "5000");
        keyspaceComboBox.setValue("Select Keyspace");
        clientIDComboBox.setValue("List of ClientID`s");

        if (type == 2){
            label.setText("Which keyspace and ClientID will be updated?");
        }else {
            label.setText("Which keyspace and ClientID will be deleted?");
        }



    }

    public String getKeyspaceComboBoxValue(){
        return keyspaceComboBox.getSelectionModel().getSelectedItem().toString();

    }
    public String getClientIDComboBoxValue(){
        //return "List of ClientID`s";
        return clientIDComboBox.getSelectionModel().getSelectedItem().toString();
    }
    private ArrayList split(String s){
        ArrayList<String> properties = new ArrayList<>();
        int start = 0;
        for (int i = 0; i < s.length(); i++){
            if (s.charAt(i) == ','){
                properties.add(s.substring(start, i ));
                start = i + 1;
            }
        }
        properties.add(s.substring(start, s.length()));
        return properties;
    }


}
