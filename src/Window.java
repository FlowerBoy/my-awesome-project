import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public abstract class Window {
    protected Stage stage = new Stage();
    protected VBox vBox;
    protected Scene scene;



    protected Window(int WIDTH, int HEIGHT) {

        vBox = new VBox(5);
        scene = new Scene(vBox,WIDTH, HEIGHT);
        stage.setTitle("OAuth2 Client Tool");


        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();

    }
}
